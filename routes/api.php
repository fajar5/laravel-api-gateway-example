<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use \Illuminate\Support\Facades\Http;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::any('{one?}/{two?}/{tree?}/{four?}/{five?}', function (Request $request) {
    $baseUrl = 'localhost:3200'; //external api endpoint
    $method = $request->method();
    if ($method == 'GET') {
        return Http::withHeaders($request->headers->all())
            ->get($baseUrl . '/' . $request->path() . '?' . $request->getQueryString())
            ->json();
    } elseif ($method == 'PATCH') {
        return Http::withHeaders($request->headers->all())
            ->patch($baseUrl . '/' . $request->path(), $request->all())
            ->json();
    } elseif ($method == 'PUT') {
        return Http::withHeaders($request->headers->all())
            ->put($baseUrl . '/' . $request->path(), $request->all())
            ->json();
    } elseif ($method == 'POST') {
        return Http::withHeaders($request->headers->all())
            ->post($baseUrl . '/' . $request->path(), $request->all())
            ->json();
    } elseif ($method == 'DELETE') {
        return Http::delete($baseUrl . '/' . $request->path())
            ->json();
    }
    return abort(405);
});
